/*
 LHS
 Copyright (C) 2018  Locchan

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "headers/json_handler.h"
#include "headers/main.h"
#include "headers/logs_handler.h"
#include "headers/server.h"

int main() {
	setbuf(stdout, NULL);
	log_info(	"\n    LSH  Copyright (C) 2018  Locchan\n"
			"    This program comes with ABSOLUTELY NO WARRANTY.\n"
			"    This is free software, and you are welcome to redistribute it\n"
			"    under certain conditions; visit <https://www.gnu.org/licenses/gpl.txt> for details.");
	logger_initialize();
	log_info("LSH v.%s started.", LSH_VERSION);
	srv_init();
/*	handle_json(parse_json(manual_data_input())); */
	while(1){
		sleep(1);
	}
}

void lsh_shutdown(int exit_code) {
	log_info("Shutting down with exit code %d...", exit_code);
	exit(exit_code);
}

