/*
 LHS
 Copyright (C) 2018  Locchan

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../ext_libs/jsmn/jsmn.h"
#include "../ext_libs/logc/log.h"
#pragma once

#ifndef SRC_HEADERS_MAIN_SRV_H_
#define SRC_HEADERS_MAIN_SRV_H_

#define MAX_JSON_SIZE 32768
#define MAX_TOKEN_COUNT 512
#define MAX_KEY_OR_VALUE_SIZE 1024

jsmntok_t *parse_json(char *json);

char *manual_data_input();

int handle_json(jsmntok_t *tokens);

#endif /* SRC_HEADERS_MAIN_SRV_H_ */
