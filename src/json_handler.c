/*
 LHS
 Copyright (C) 2018  Locchan

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "headers/json_handler.h"
#include "headers/main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char JSON_STRING[MAX_JSON_SIZE];
jsmntok_t tokens[MAX_TOKEN_COUNT];
int token_count = 0;
int parsed_json = 0;

/*char *manual_data_input() {
	printf("Enter the data manually: ");
	char *input_data = (char *) malloc(MAX_JSON_SIZE);
	if (input_data == NULL) {
		printf("NO MEMORY");
		exit(1);
	}
	fgets(input_data, MAX_JSON_SIZE, stdin);
	return input_data;
}*/

jsmntok_t *parse_json(char *json) {
	strcpy(JSON_STRING, json);
	jsmn_parser parser;
	jsmn_init(&parser);
	log_debug("Parsing json: %s", json);
	parsed_json = jsmn_parse(&parser, json, strlen(json), tokens,
			MAX_TOKEN_COUNT);
	switch (parsed_json) {
	case (JSMN_ERROR_INVAL):
	case (JSMN_ERROR_PART):
		log_error("COULD NOT PARSE JSON: Invalid JSON!");
		return NULL;
		break;
	case (JSMN_ERROR_NOMEM):
		log_error("COULD NOT PARSE JSON: Not enough memory (or too much tokens)!");
		return NULL;
		break;
	}
	if (tokens[0].type != JSMN_OBJECT) {
		log_error("JSON DATA IS INVALID!");
		return NULL;
	}
	token_count = parsed_json;
	log_debug("Got %d tokens", parsed_json);
	return tokens;
}

int handle_json(jsmntok_t *tokens) {
	if(tokens==NULL) return -1;
	char *value;
	value = (char*) malloc(MAX_KEY_OR_VALUE_SIZE * sizeof(char) + 1);
	char *key;
	key = (char*) malloc(MAX_KEY_OR_VALUE_SIZE * sizeof(char) + 1);
	int i;
	for (i = 1; i < parsed_json; i += 2) {
		jsmntok_t json_value = tokens[i + 1];
		jsmntok_t json_key = tokens[i];
		int string_length = json_value.end - json_value.start;
		int key_length = json_key.end - json_key.start;
		int idx;
		for (idx = 0; idx < string_length; idx++) {
			value[idx] = JSON_STRING[json_value.start + idx];
		}
		for (idx = 0; idx < key_length; idx++) {
			key[idx] = JSON_STRING[json_key.start + idx];
		}
		value[string_length] = '\0';
		key[key_length] = '\0';
		log_debug("KEY: %s; VALUE: %s", key, value);
	}
	return 0;
}

