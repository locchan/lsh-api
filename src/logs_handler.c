/*
 LHS
 Copyright (C) 2018  Locchan

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include "headers/logs_handler.h"

void logger_initialize() {
	FILE *fp;
	fp = fopen("LSH.log", "a");
	if (fp == NULL) {
	    log_error("Could not open log file for writing");
	}
	log_set_fp(fp);
}
