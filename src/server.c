/*
 LHS
 Copyright (C) 2018  Locchan

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#include "headers/main.h"
#include "headers/logs_handler.h"
#include "headers/json_handler.h"
#include "headers/server.h"

static void *initialize_server() {
	int socket_desc, client_sock, c, read_size;
	struct sockaddr_in server, client;
	char client_message[MAX_JSON_SIZE];

	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1) {
		log_error("Could not create socket");
		lsh_shutdown(1);
	}
	log_info("Socket created");

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(37529);

	if (bind(socket_desc, (struct sockaddr *) &server, sizeof(server)) < 0) {
		log_error("Bind failed. Error");
		lsh_shutdown(1);
	}
	log_info("Bind done");

	listen(socket_desc, 3);

	log_info("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);

	client_sock = accept(socket_desc, (struct sockaddr *) &client,
			(socklen_t*) &c);
	if (client_sock < 0) {
		log_warn("Accept failed");
	}
	log_info("Connection accepted");

	while ((read_size = recv(client_sock, client_message, 2000, 0)) > 0) {
		log_debug("Got data \"%s\" from socket %d", client_message, client_sock);
		if(handle_json(parse_json(client_message)) == 0){
			respond_to_client(client_sock, "Successfully parsed");
		} else respond_to_client(client_sock, "Could not parse JSON");
	}
	if (read_size == 0) {
		log_info("Client disconnected");
		fflush(stdout);
	} else if (read_size == -1) {
		log_error("Recv failed");
	}
	return NULL;
}

void srv_init() {
	log_info("Trying to initialize server...");
	pthread_t thread_id;
	pthread_create(&thread_id, NULL, initialize_server, NULL);
}

void respond_to_client(int socket, char* message){
	log_debug("Sending \"%s\" to socket %d", message, socket);
	write(socket , message , strlen(message));
}
