################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/json_handler.c \
../src/logs_handler.c \
../src/main.c \
../src/server.c 

OBJS += \
./src/json_handler.o \
./src/logs_handler.o \
./src/main.o \
./src/server.o 

C_DEPS += \
./src/json_handler.d \
./src/logs_handler.d \
./src/main.d \
./src/server.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C Compiler'
	gcc -std=c90 -Os -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<" -DLOG_USE_COLOR
	@echo 'Finished building: $<'
	@echo ' '


